#!/bin/bash

echo "Removing all but Git items."

find . ! \( -path "./.git*" -o -path "./gitkeep.sh" -o -path "." -o -path ".." \) -exec rm -rf {} +

cp -r ../monkey-blog/public/* .
